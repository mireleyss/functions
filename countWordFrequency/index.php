<?php

    //три варианта, с использованием встроенных функций и без
    function countWordFrequencyFirst(string $string): array
    {
        $string = strtolower($string);
        $string = preg_replace("/[\s,.!?;:()]+/", ' ', $string);
        $string = trim($string);
        $words_apart = explode(' ', $string);

        return array_count_values($words_apart);
    }

    function countWordFrequencySecond(string $string): array
    {
        $string = strtolower($string);
        $string = preg_replace("/[\s,.!?;:()]+/", ' ', $string);
        $string = trim($string);
        $words_apart = explode(' ', $string);

        $words_frequency = array();

        foreach ($words_apart as $word) {
            $repetition = count(array_keys($words_apart, $word));
            $words_frequency += [$word => $repetition];
        }

        return $words_frequency;
    }

    function countWordFrequencyThird(string $string): array
    {
        $string = strtolower($string);
        $string = preg_replace("/[\s,.!?;:()]+/", ' ', $string);
        $string = trim($string);
        $words_apart = explode(' ', $string);

        $words_frequency = array();

        for ($i = 0; $i < count($words_apart); $i++) {
            $repetition = 1;

            for ($j = $i + 1; $j < count($words_apart); $j++) {
                if ($words_apart[$i] == $words_apart[$j]) {
                    $repetition++;
                }
            }

            $words_frequency += [$words_apart[$i] => $repetition];
        }

        return $words_frequency;
    }

    $string = "  . The quick brown brown fox fox,    jumps over the:lazy lazy dog...    ";
    print_r(countWordFrequencyFirst($string));
    echo "<br>";
    print_r(countWordFrequencySecond($string));
    echo "<br>";
    print_r(countWordFrequencyThird($string));

